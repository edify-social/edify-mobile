# Edify Mobile

## Installation (debug)

* Check device `adb devices -l`
* Build android package `ionic cordova build --release android`
* Install package `adb install ~/Edify/edify-mobile/platforms/android/build/outputs/apk/android-debug.apk`
* Reinstall package `adb install -r ~/Edify/edify-mobile/platforms/android/build/outputs/apk/android-debug.apk`
* If you hit permission errors use: `sudo chmod -R a+w /Users/josetorres/Edify/edify-mobile/platforms/android/gradle/wrapper/`

## Debug
Launch the app on your Android using `sudo ionic cordova run android -lcs` Open Chrome and go to: `chrome://inspect` - this will list your connected devices.
Inspect the app using chrome dev tools as if you were inspecting any other web page.  
When running the apk through CLI you receive `Failure [INSTALL_FAILED_UPDATE_INCOMPATIBLE]` you'll need to run `adb uninstall social.edify.mobile`
* In case `adb` is not found we can run `. ~/.bashrc`

To fix `EACCESS` When installing plugins change permissions such as:

```
sudo chown joto:joto /home/joto/workspace/Edify/edify-mobile/plugins
```

## Release

1. Increase version number on the widget tag within `config.xml`
2. Build for release with `sudo ionic cordova build --release android` or for Linux use  `cordova build --release android` 
3. From the project root directory run `cp platforms/android/build/outputs/apk/android-release-unsigned.apk ..`
4. Run `cd ..` and remove any apk file from previous releases from this directory.
5. Sign the apk with `jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ~/Dropbox/Edify/keys/edify-child-release-key.keystore android-release-unsigned.apk alias_name`
6. Passphrase for key is in 1Password
7. Optimize the apk with `/Users/josetorres/Library/Android/sdk/build-tools/26.0.1/zipalign -v 4 android-release-unsigned.apk EdifyMobile.apk` or for Linux use `zipalign -v 4 android-release-unsigned.apk EdifyMobile.apk` 
8. Open the [release manager](https://play.google.com/apps/publish/?dev_acc=06645867455284751902#ManageReleasesPlace:p=social.edify.mobile) in you browser.
9. Create a new release and upload the apk.

## Release notes
* Cordova plugin was 0.6 but has been updated for 1.0

## Android SMS Message Codes

```
MESSAGE_TYPE_ALL    = 0;
MESSAGE_TYPE_INBOX  = 1;
MESSAGE_TYPE_SENT   = 2;
MESSAGE_TYPE_DRAFT  = 3;
MESSAGE_TYPE_OUTBOX = 4;
MESSAGE_TYPE_FAILED = 5; // for failed outgoing messages
MESSAGE_TYPE_QUEUED = 6; // for messages to send later
```
