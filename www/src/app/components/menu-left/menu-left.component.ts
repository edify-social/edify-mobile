import { Component, OnInit } from '@angular/core';
declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'cat-menu-left',
  templateUrl: './menu-left-vertical.component.html',
})
export class MenuLeftComponent implements OnInit {
  ngOnInit() {

    $(function(){

      

      /////////////////////////////////////////////////////////////////////////////////////////
      // custom scroll init

      if ($('body').hasClass('cat__config--vertical')) {
        if (!(/Mobi/.test(navigator.userAgent)) && jQuery().jScrollPane) {
          $('.cat__menu-left__inner').each(function () {
            $(this).jScrollPane({
              contentWidth: '0px',
              autoReinitialise: true,
              autoReinitialiseDelay: 100
            });
            var api = $(this).data('jsp'),
              throttleTimeout;
            $(window).bind('resize', function () {
              if (!throttleTimeout) {
                throttleTimeout = setTimeout(function () {
                  api.reinitialise();
                  throttleTimeout = null;
                }, 50);
              }
            });
          });
        }
      }


      /////////////////////////////////////////////////////////////////////////////////////////
      // toggle menu

      $('.cat__menu-left__action--menu-toggle').on('click', function(){
        if ($('body').width() < 768) {
          $('body').toggleClass('cat__menu-left--visible--mobile');
        } else {
          $('body').toggleClass('cat__menu-left--visible');
        }
      })

      $('.cat__menu-left__action--backdrop-toggle').on('click', function(){
        $('body').removeClass('cat__menu-left--visible--mobile');
      })


    });

  }
}
