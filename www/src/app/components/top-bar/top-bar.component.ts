import { Component } from '@angular/core';
declare var $: any;
declare var jQuery: any;


@Component({
  selector: 'cat-top-bar',
  templateUrl: './top-bar.component.html',
})
export class TopBarComponent {
  ngOnInit() {

    $(function(){
       $('.cat__menu-left__pin-button').on('click', function(){
          $('body').toggleClass('cat__menu-left--visible--mobile');
       });
       $('[data-toggle="tooltip"]').tooltip();
    });
  }
}
