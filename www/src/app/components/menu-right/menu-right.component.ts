import { Component, OnInit } from '@angular/core';
declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'cat-menu-right',
  templateUrl: './menu-right.component.html',
})
export class MenuRightComponent implements OnInit {
  ngOnInit() {

    $(function(){

      $('.cat__menu-right__action--menu-toggle').on('click', function(){
        $('body').removeClass('cat__menu-right1--visible');
        $('body').toggleClass('cat__menu-right--visible');
      });

      $('.cat__menu-right1__action--menu-toggle').on('click', function(){
        
        $('body').removeClass('cat__menu-right--visible');
        $('body').toggleClass('cat__menu-right1--visible');


        var isRight = $( "body" ).hasClass( "cat__menu-right1--visible" ).toString();
        if(isRight==true){
            $('.baricon').css({'display':'none'});
            $('.baricon_right').css({'display':'block'});
        }else{
            $('.baricon').css({'display':'block'});
            $('.baricon_right').css({'display':'none'});
        }


      });


      if (!(/Mobi/.test(navigator.userAgent)) && jQuery().jScrollPane) {
        $('.cat__top-bar__activity').each(function () {
          $(this).jScrollPane({
            contentWidth: '0px',
            autoReinitialise: true,
            autoReinitialiseDelay: 100
          });
          var api = $(this).data('jsp'),
            throttleTimeout;
          $(window).bind('resize', function () {
            if (!throttleTimeout) {
              throttleTimeout = setTimeout(function () {
                api.reinitialise();
                throttleTimeout = null;
              }, 50);
            }
          });
        });
      }

    });
    
  }
}
