import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';

import { DashboardsModule } from './dashboards/dashboards.module';

@NgModule({
  imports: [
    CommonModule,
    DashboardsModule
  ]
})
export class StructureModule { }
