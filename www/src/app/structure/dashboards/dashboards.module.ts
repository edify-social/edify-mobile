import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { Routes, RouterModule }  from '@angular/router';

import { DashboardsPeople } from './people.page';

export const routes: Routes = [
  { path: 'dashboards/people', component: DashboardsPeople },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    DashboardsPeople
  ]

})

export class DashboardsModule { }
