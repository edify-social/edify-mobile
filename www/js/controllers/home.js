angular.module('edify.controller.home',[])
  .controller('homeCtrl',function(
    $scope,
    $http,
    $ionicPlatform,
    AppsService,
    BackendRequestsService
  ){

    // Variables
    $scope.pin = window.localStorage.getItem("ChildPin");
    $scope.sms_msgs_pre = JSON.parse(window.localStorage.getItem("SmsIDs"));
    if($scope.sms_msgs_pre == null){
      $scope.sms_msgs = [];
    } else {
      $scope.sms_msgs = $scope.sms_msgs_pre;
    }// List of sms msg ids

    $ionicPlatform.ready(function(){

      // permission
      var permissions = cordova.plugins.permissions;
      permissions.requestPermission(permissions.READ_SMS, success, error);
      function error() {
        console.warn('SMS permission is not turned on');
      }
      function success( status ) {
        if( !status.hasPermission ) error();
      }

      $ionicPlatform.ready(function(){
        if(SMS) SMS.setOptions({
          license: "jose.r.torres55@gmail.com/49b3d05b78b8e6d6377eedacb7ecd578"
        }, function(data){
          console.log("success license");
          console.log(data);
        }, function(err){
          console.log("error license");
          console.log(err);
        });
      });

      /******
       * Store ids for sent messages
       ******/
      $scope.listSMS = function () {
        var filter = {
          box: '',
          maxCount: 10000
        };

        var ctcs = JSON.parse(window.localStorage.getItem("contacts"));

        if(SMS) SMS.listSMS(filter, function(data){
          $scope.sms_pruned = []; 	// All msgs that will be sent to server
          $scope.install_time = Number(window.localStorage.getItem("install_date"));

          angular.forEach(data, function(val){
            if($scope.sms_msgs.indexOf(val._id) < 0){
              // Iterate through each value in data and add it´s ID to sms_msgs
              // if not already there. Also create a new data object without any
              // other msg box

              if(val.date >= $scope.install_time){
                // Store ids even for messages from other boxes i.e. drafts
                $scope.sms_msgs.push(val._id);

                // Store msgs only from in and out boxes
                if(val.type <= 2){
                  $scope.sms_pruned.push(val);
                }
              }
            }
          });
          window.localStorage.setItem("SmsIDs", JSON.stringify($scope.sms_msgs));
          //window.localStorage.setItem("SmsList", JSON.stringify($scope.sms_pruned));

          // Get all contacts for sms list
          angular.forEach($scope.sms_pruned, function(tval, key){
            ctct = tval.address;
            if(Number(ctct)){
              norm_ctct = $scope.normalize_string(tval.address);
              angular.forEach(ctcs, function(val){
                angular.forEach(val.phoneNumbers, function(subval) {
                  if($scope.normalize_string(subval.value) == norm_ctct){
                    tval.contact_name = val.displayName;
                  }
                });
              });
            } else {
              console.log("not found");
              tval.contact_name = tval.address;
            }
          });

          // Push
          if($scope.sms_pruned.length > 0) {
            BackendRequestsService.publishMessages($scope.sms_pruned, $scope.pin, $scope.sms_msgs);
          }
        }, function(err){
          console.log('error list sms: ' + err);
        });
      }; // -- END SMSList

      $scope.normalize_string = function (s) {
        new_string = s.replace(/-|\s|\+/g,"");
        new_string = s.substr(s.length - 6);
        return new_string
      };

      /******
       * Store ids for backtracked messages
       ******/
      $scope.listSMSBacktrack = function() {
        var filter = {
          box: '',
          maxCount: 10000
        };

        if(SMS) SMS.listSMS(filter, function(data){
          $scope.sms_pruned_backtrack = []; 	// All msgs that will be sent to server
          $scope.sms_msgs_back 	= []; 	// List of sms msg ids

          var ctcs = JSON.parse(window.localStorage.getItem("contacts"));

          angular.forEach(data, function(val){
            // Store ids even for messages from other boxes i.e. drafts
            $scope.sms_msgs_back.push(val._id);
            // Store msgs only from in and out boxes
            if(val.type <= 2){
              $scope.sms_pruned_backtrack.push(val);
            }
          });
          window.localStorage.setItem("SmsIDsBack", $scope.sms_msgs_back);
          window.localStorage.setItem("SmsListBack", JSON.stringify($scope.sms_pruned_backtrack));

          window.pre_pruned = $scope.sms_pruned_backtrack;

          angular.forEach($scope.sms_pruned_backtrack, function(tval, key){
            ctct = tval.address;
            if(Number(ctct)){
              norm_ctct = $scope.normalize_string(tval.address);
              angular.forEach(ctcs, function(val){
                angular.forEach(val.phoneNumbers, function(subval) {

                  if($scope.normalize_string(subval.value) == norm_ctct){
                    tval.contact_name = val.displayName;
                    //tval.raw = subval.value + ' - ' + tval.address;
                  }
                });
                tval.raw = val.phoneNumbers;
              });
            } else {
              console.log("not found");
              tval.contact_name = tval.address;
            }
          });

          // Push
          BackendRequestsService.publishMessagesBacktrack($scope.sms_pruned_backtrack, $scope.pin);
        });
      };// -- END SMSListBacktrack

      /******
       * Backtrack Check
       ******/
      $scope.backtrackCheck = function () {
        var url = "https://api.edify.social/v1/backtrack_check";
        $scope.dta = {
          pin: $scope.pin
        };
        return $http({
          method: 'POST',
          url: url,
          data: $scope.dta
        }).success(appSuccessFn).error(appErrorFn);

        function appSuccessFn(data) {
          window.localStorage.setItem("backtrack", data.data.backtrack);
          window.localStorage.setItem("backtrack_status", data.data.status);
          $scope.backtrack = data.data.backtrack;
          $scope.backtrack_status = data.data.status;
          if($scope.backtrack == true && $scope.backtrack_status == 0){
            $scope.listSMSBacktrack();
          }
        }
        function appErrorFn(data, error, status, headers, config) {
          console.error('Backtrack check: ' + error + ', ' + JSON.stringify(data));
        }
      };

      // Function calls
      $scope.backtrackCheck();
      window.bk = $scope.backtrack;
      window.st = $scope.backtrack_status;

      $scope.listSMS();
      AppsService.getAppList($scope.pin);

    });

    document.addEventListener('deviceready', function () {

      // Enable Auto Start
      cordova.plugins.autoStart.enable();

      // Background mode
      cordova.plugins.backgroundMode.setDefaults({
        title:  'Edify Sync',
        text:   'Managing Server Sync'
      });
      cordova.plugins.backgroundMode.excludeFromTaskList();

      cordova.plugins.backgroundMode.enable();

      var refreshIntervalId = 0;
      var interval_minutes = 900000; // 15 minutes

      cordova.plugins.backgroundMode.onactivate = function () {
        if(cordova.plugins.backgroundMode.isActive()){

          refreshIntervalId = setInterval(function () {
            console.log("interval id: " + refreshIntervalId);

            // Function calls
            $scope.backtrackCheck();
            window.bk = $scope.backtrack;
            window.st = $scope.backtrack_status;
            $scope.listSMS();
            AppsService.getAppList($scope.pin);

          }, interval_minutes);
        }
      };

      cordova.plugins.backgroundMode.ondeactivate = function () {
        if(!cordova.plugins.backgroundMode.isActive()){
          if(refreshIntervalId) {
            console.log("int id: " + refreshIntervalId);
            clearInterval(refreshIntervalId);
          } else {
            // User probably has app in foreground which should trigger a single
            // sync and a recurrent sync should not be necessary but if it is
            // this is where it will go.
          }
        }
      };

    }, false);

  });
