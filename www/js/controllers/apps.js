angular.module('edify.controller.apps',[])
.controller('appsCtrl',function(
	$scope,
	$ionicPlatform,
	$ionicHistory,
	$ionicPopup,
  $http
){
	$ionicPlatform.ready(function(){
		try{
			$ionicHistory.nextViewOptions({
				disableBack: true
			});
			$ionicHistory.clearHistory();
		}catch(err){
			console.log(err.message);
		}
	});

  $scope.pin = window.localStorage.getItem("ChildPin");

  /******
   * Backtrack Check
   ******/
  $scope.backtrackCheck = function () {
    var url = "https://api.edify.social/v1/backtrack_check";
    $scope.dta = {
      pin: $scope.pin
    };
    return $http({
      method: 'POST',
      url: url,
      data: $scope.dta
    })
      .success(appSuccessFn).error(appErrorFn);

    function appSuccessFn(data) {
      window.localStorage.setItem("backtrack", data.data.backtrack);
      window.localStorage.setItem("backtrack_status", data.data.status);
      $scope.backtrack = data.data.backtrack;
      $scope.backtrack_status = data.data.status;
    }
    function appErrorFn(data, error, status, headers, config) {
      console.error('Backtrack check: ' + error + ', ' + JSON.stringify(data));
    }
  };

  // Function calls
  $scope.backtrackCheck();
  window.bk = $scope.backtrack;
  window.st = $scope.backtrack_status;

	$scope.appList = JSON.parse(window.localStorage.getItem("AppList"));
});
