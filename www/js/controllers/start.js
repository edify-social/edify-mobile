angular.module('edify.controller.start',[])
.controller('startCtrl',function(
	$scope,
	$ionicPopup,
	$ionicPlatform,
	$ionicHistory,
	$ionicLoading,
	$http,
	$state,
	$localstorage,
  ContactsService
){

	// Login automatically if pin is set
	$scope.pin = window.localStorage.getItem("ChildPin");

	if($scope.pin){
		$state.go('home');
	}

	$scope.data={};
	$scope.data.pin="";

	$scope.getInstallDate = function() {
	  current_time = Date.now();
	  install_date = window.localStorage.getItem("install_date");

	  if(install_date == null) {
      window.localStorage.setItem("install_date", Date.now());
    }
  };

	$scope.LogIn = function(){
		$ionicLoading.show();
		var url = "https://api.edify.social/v1/child_onboarding/activate_android/";

		$scope.dt = {
				pin: $scope.data.pin
		};

		$http.post(url, $scope.dt).then(function(res){
			$scope.userinfo=res.data.data.msg;
			window.localStorage.setItem("Userinfo", JSON.stringify($scope.userinfo));
			window.localStorage.setItem("ChildName", $scope.userinfo.name);
			window.localStorage.setItem("ChildPin", $scope.userinfo.pin);
			$ionicLoading.hide();
			$state.go('home');
		});
	};

  /******
   * Backtrack Check
   ******/
  $scope.backtrackCheck = function () {
    var url = "https://api.edify.social/v1/backtrack_check";
    $scope.dta = {
      pin: $scope.pin
    };
    return $http({
      method: 'POST',
      url: url,
      data: $scope.dta
    })
      .success(appSuccessFn).error(appErrorFn);

    function appSuccessFn(data) {
      window.localStorage.setItem("backtrack", data.data.backtrack);
      window.localStorage.setItem("backtrack_status", data.data.status);
      $scope.backtrack = data.data.backtrack;
      $scope.backtrack_status = data.data.status;
    }
    function appErrorFn(data, error, status, headers, config) {
      console.error('Backtrack check: ' + error + ', ' + JSON.stringify(data));
    }
  };


  // Function calls
  $scope.backtrackCheck();
  window.bk = $scope.backtrack;
  window.st = $scope.backtrack_status;

  $scope.getInstallDate();

  $ionicPlatform.ready(function() {
    ContactsService.getEveryContact();
  });
});
