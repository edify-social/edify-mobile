angular.module('edify.controller.messages',[])
.controller('messageCtrl',function(
	$scope,
	$ionicPlatform,
	$ionicHistory,
  $http
){
	$ionicPlatform.ready(function(){

		var permissions = cordova.plugins.permissions;
    $scope.pin = window.localStorage.getItem("ChildPin");
    permissions.requestPermission(permissions.READ_SMS, success, error);

		function error() {
			console.warn('SMS permission is not turned on');
		}

		function success( status ) {
			if( !status.hasPermission ) error();
		}

		$ionicHistory.nextViewOptions({
			disableBack: true
		});
		$ionicHistory.clearHistory();

    $scope.messages = JSON.parse(window.localStorage.getItem("SmsList"));

    /******
     * Backtrack Check
     ******/
    $scope.backtrackCheck = function () {
      var url = "https://api.edify.social/v1/backtrack_check";
      $scope.dta = {
        pin: $scope.pin
      };
      return $http({
        method: 'POST',
        url: url,
        data: $scope.dta
      })
        .success(appSuccessFn).error(appErrorFn);

      function appSuccessFn(data) {
        window.localStorage.setItem("backtrack", data.data.backtrack);
        window.localStorage.setItem("backtrack_status", data.data.status);
        $scope.backtrack = data.data.backtrack;
        $scope.backtrack_status = data.data.status;
      }
      function appErrorFn(data, error, status, headers, config) {
        console.error('Backtrack check: ' + error + ', ' + JSON.stringify(data));
      }
    };


    // Function calls
    $scope.backtrackCheck();
    window.bk = $scope.backtrack;
    window.st = $scope.backtrack_status;
	});
});
