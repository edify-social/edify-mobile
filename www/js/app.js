
angular.module('edify', [
  'ionic',
  'ngCordova',
  'edify.controllers',
  'edify.services'
])

.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}])
.config(function($ionicConfigProvider) {
  $ionicConfigProvider.navBar.alignTitle('center');
})

.config(['$ionicConfigProvider', function($ionicConfigProvider) {
 $ionicConfigProvider.tabs.position('bottom');
}])

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

  .state('nav', {
    cache: false,
      url: '/nav',
      templateUrl: 'templates/nav.html',
      controller: 'navCtrl'
    })

  .state('start', {
    cache: false,
      url: '/start',
      templateUrl: 'templates/start.html',
      controller: 'startCtrl'
    })

   .state('messages', {
    cache: false,
      url: '/messages',
      templateUrl: 'templates/message.html',
      controller: 'messageCtrl'
    })

   .state('apps', {
    cache: false,
      url: '/apps',
      templateUrl: 'templates/apps.html',
      controller: 'appsCtrl'
    })

   .state('home', {
    cache: false,
      url: '/home',
      templateUrl: 'templates/home.html',
      controller: 'homeCtrl'
    });

 $urlRouterProvider.otherwise('/start');

});
