var mod = angular.module('edify.services', [
  'edify.services.contacts',
  'edify.services.sms',
  'edify.services.apps',
  'edify.services.backend_requests'
]);
