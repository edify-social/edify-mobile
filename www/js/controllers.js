var mod = angular.module('edify.controllers', [
  'edify.controller.apps',
  'edify.controller.messages',
  'edify.controller.home',
  'edify.controller.nav',
  'edify.controller.start'
]);
