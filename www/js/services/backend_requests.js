var mod = angular.module('edify.services.backend_requests', []);


mod.service('BackendRequestsService', function (
  $http
) {

  /****************************************************************************
   *
   * Contacts
   *
   ****************************************************************************/

  var self = {
    post: function (url, data) {
      return $http({
        method: 'POST',
        url: url,
        data: data
      }).success(appSuccessFn).error(appErrorFn);

      function appSuccessFn(data) {
        return data;
      }

      function appErrorFn(data, error, status, headers, config) {
        console.error('Server error: ' + error + ', ' + JSON.stringify(data));
      }
    },
    publishMessagesBacktrack: function (payload, pin) {
      var url 					= "https://api.edify.social/v1/backtrack";
      var sms_list 	= {
        pin: pin,
        messages: payload
      };
      return $http({
        method: 'POST',
        url: url,
        data: sms_list
      })
        .success(registerSuccessFn).error(registerErrorFn);

      function registerSuccessFn(data) {
        console.log(data);
      }

      function registerErrorFn(data, error, status, headers, config) {
        console.error('Register error: ' + error + ', ' + JSON.stringify(data));
      }
    },
    publishMessages: function (payload, pin, sms_msgs) {
      var url 					= "https://api.edify.social/v1/device_messages";
      var lgth 					= sms_msgs.length - 1;
      //console.log(sms_msgs);
      var sms_list 	= {
        pin: pin,
        messages: payload
      };
      //console.log(sms_list);
      window.sm = sms_msgs;
      if(sms_msgs.length > 0){
        if(sms_msgs.indexOf(Number(sms_msgs[lgth]))) {
          console.log("publishMessage send");
          // Comment out to avoid server saturation
          return $http({
            method: 'POST',
            url: url,
            data: sms_list
          })
            .success(registerSuccessFn).error(registerErrorFn);

          function registerSuccessFn(data) {
            console.log(data);
          }

          function registerErrorFn(data, error, status, headers, config) {
            console.error('Register error: ' + error + ', ' + JSON.stringify(data));
          }
          // -- Comment out
        }
      }
    }
  };

  return self;
});
