var mod = angular.module('edify.services.apps', []);

mod.service('AppsService', function (
  BackendRequestsService
) {

  /****************************************************************************
   *
   * Contacts
   *
   ****************************************************************************/

  var self = {

    /******
     * Get and send application list
     ******/
    getAppList: function (pin) {
      var success = function(app_list) {
        var apps = [];
        angular.forEach(app_list, function(val){
          apps.push(val.name);
        });
        window.localStorage.setItem("AppList", JSON.stringify(app_list));

        var url = "https://api.edify.social/v1/app_list";
        var app_list_data = {
          pin: pin,
          applications: apps
        };

        BackendRequestsService.post(url, app_list_data);

      };
      var error = function(app_list) {
        alert("Something went wrong requesting the application list " + app_list);
      };

      var no_of_days = 7;
      var a = new Date();
      var b = new Date(a.setTime(a.getTime() + no_of_days * 86400000));
      Applist.createEvent('', '', '', a, b, success, error)
    }
  };

  return self;
});
