var mod = angular.module('edify.services.contacts', []);


mod.service('ContactsService', function (
) {

  /****************************************************************************
   *
   * Contacts
   *
   ****************************************************************************/

  var self = {
    getEveryContact: function(){
      function onSuccess(contacts) {
        window.localStorage.setItem("contacts", JSON.stringify(contacts));
        window.ctcs = JSON.parse(window.localStorage.getItem("contacts"));
      }

      function onError(contactError) {
        alert('onError!');
      }

      // find all contacts
      var options = new ContactFindOptions();
      options.filter = "";
      options.multiple = true;
      options.hasPhoneNumber = true;
      var filter = ["displayName", "phoneNumbers"];
      navigator.contacts.find(filter, onSuccess, onError, options);
    },
    findContact: function(phone) {
      var ctcs = JSON.parse(window.localStorage.getItem("contacts"));

      angular.forEach(ctcs, function(val){
        angular.forEach(val.phoneNumbers, function(subval) {
          if(subval.value == phone){
            return val.displayName;
          }
        });
      });
    },

    getAllContactsBacktrack: function (sms_list) {
      console.log("getallcontacts backtrack called");
      var ctcs = JSON.parse(window.localStorage.getItem("contacts"));

      angular.forEach(sms_list, function(val, key){
        ctct = val.address;
        if(Number(ctct)){
          console.log(val.address);

          val.contact_name = self.findContact(val.address, key);

          console.log("getall");
          console.log(self.findContact(val.address));
        } else {
          console.log("not found");
          val.contact_name = val.address;
        }
      });
      window.pruned = sms_list;
      return sms_list;
    },
    getAllContacts: function (sms_list) {
      console.log("getallcontacts called");

      angular.forEach(sms_list, function(val){
        ctct = val.address;
        if(Number(ctct)){
          val.contact_name = self.findContact(val.address);
        } else {
          val.contact_name = val.address;
        }
      });
      window.pruned = sms_list;
      return sms_list;
    }

};

  return self;
});
